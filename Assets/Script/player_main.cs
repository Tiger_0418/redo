﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class player_main : MonoBehaviour {

	public float 血量 = 100;

	public float 走路速度 = 5f;
	public float 跳躍高度 = 700f;
	public float 前斬傷害 = 10f;
	public float 旋斬傷害 = 10f;
	public float 上砍傷害 = 10f;
	public float key_Q = 0.3f;
	public float key_W = 0.3f;
	public float key_E = 0.3f;

	Animator ani;
	Rigidbody2D rig;

	public bool jump;
	public bool left;
	public bool right;
//	public bool idle;
	public bool attack_1;
	public bool attack_2;
	public bool attack_3;
	public bool hit;
	public bool die;
	int hit_check = 0;
	public int attack_check = 0;
	int jumpatt_check = 0;

	public float time_f;
	public float time_fe;

	GameObject shadow;
	public float money = 0;

	int bron = 0;
	Vector3 movedis;

	public GameObject att_par_1;
	public GameObject att_par_2;
	public GameObject att_par_3;
	public GameObject jump_par;
	public int jump_par_check = 0;

//	public List<Vector3> checkpoint = new List<Vector3>();
	public List<float> timepoint = new List<float>();
	public List<string> button = new List<string>();

	// Use this for initialization
	void Start () 
	{
		ani = this.GetComponent<Animator> ();
		rig = this.GetComponent<Rigidbody2D> ();

		//DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (血量 == 0)
		{
			die = true;
		}
		if (die)
		{
			StartCoroutine (restart_wait());
		}
		else
		{
			ani_fn ();

			if (hit && hit_check == 0)
			{
				if (this.transform.localEulerAngles.y == 0)
				{
					rig.AddRelativeForce (Vector3.left*300f);
				}
				if (this.transform.localEulerAngles.y == 180)
				{
					rig.AddRelativeForce (Vector3.right*300f);
				}
				rig.AddRelativeForce (Vector3.up*300f);
				left = false;
				right = false;
				attack_1 = false;
				attack_2 = false;
				attack_3 = false;
				StartCoroutine (hit_cor());
				hit_check = 1;
			}

			if (attack_1 && attack_check == 0) 
			{
				if (left) 
				{
					movedis = new Vector3 (transform.position.x-50f,transform.position.y,transform.position.z);
					attparti_1 (att_par_1,0.7f);
					StartCoroutine (att1_wait_fin ());
					attack_check = 1;
				}
				if (right) 
				{
					movedis = new Vector3 (transform.position.x+50f,transform.position.y,transform.position.z);
					attparti_1 (att_par_1,0.7f);
					StartCoroutine (att1_wait_fin ());
					attack_check = 1;
				} 
				if (right == false && left == false) 
				{
					attparti_1 (att_par_1,0.7f);
					StartCoroutine (att1_wait_fin ());
					attack_check = 1;
				}
			} 
			if (attack_2 && attack_check == 0) 
			{
				if (left || right) 
				{
					attparti_1 (att_par_2,0.6f);
					StartCoroutine (att2_wait_fin ());
					if (jump == false)
					{
						left = false;
					}
					attack_check = 1;
				}
				else 
				{
					attparti_1 (att_par_2,0.6f);
					StartCoroutine (att2_wait_fin ());
					attack_check = 1;
				}
			}
			if (attack_3 && attack_check == 0) 
			{
				if (left || right) 
				{
					attparti_1 (att_par_3,0.4f);
					StartCoroutine (att3_wait_fin ());
					if (jump == false)
					{
						left = false;
					}
					attack_check = 1;
				}
				else 
				{
					attparti_1 (att_par_3,0.4f);
					StartCoroutine (att3_wait_fin ());
					attack_check = 1;
				}
			}
			if (right)
			{
				transform.Translate (Vector3.right * 走路速度 * Time.deltaTime,Space.World);
				this.transform.eulerAngles = new Vector3 (0,0,0);
			}
			if (left)
			{
				transform.Translate (Vector3.left * 走路速度 * Time.deltaTime, Space.World);
				this.transform.eulerAngles = new Vector3 (0, 180, 0);
			}
			if (attack_1 && left || attack_1 && right) 
			{
				transform.position = Vector3.Lerp (transform.position,movedis,0.001f);
			}
			if (jump && jump_par_check == 0)
			{
				GameObject att_par_1_cn = Instantiate (jump_par);
				att_par_1_cn.transform.localPosition = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
				Destroy (att_par_1_cn,1f);

				jump_par_check = 1;
			}

			//幻影使用
			if (this.tag == "Shadow")
			{
				time_f = time_f + 1f * Time.deltaTime;
				time_fe = Mathf.Floor (time_f*100);
				if (time_fe >= timepoint[0] && timepoint.Count >= 1)
				{
					if (button[bron] == "LeftArrow")
					{
						left = true;
					}
					if (button[bron] == "LeftArrowUp")
					{
						left = false;
					}
					if (button[bron] == "RightArrow")
					{
						right = true;
					}
					if (button[bron] == "RightArrowUp")
					{
						right = false;
					}
					if (button[bron] == "attack_1")
					{
						attack_1 = true;
					}
					if (button[bron] == "attack_2")
					{
						attack_2 = true;
					}
					if (button[bron] == "attack_3")
					{
						attack_3 = true;
					}
					if (button[bron] == "jump")
					{
						rig.AddRelativeForce (Vector2.up * 跳躍高度);
					}
					timepoint.RemoveAt (0);
					button.RemoveAt (0);
				}
				if (timepoint.Count == 0)
				{
					this.tag = "shadow_die";
					Destroy (this.gameObject, 1f);
				}
			}

			//主角使用
			if (this.tag == "Player")
			{
				time_f = time_f + 1f * Time.deltaTime;
				time_fe = Mathf.Floor (time_f*100);
				if (Input.GetKey(KeyCode.LeftArrow) && attack_1 == false && attack_2 == false && attack_3 == false && left == false && hit == false)
				{
					left = true;
					button.Add ("LeftArrow");
					timepoint.Add (time_fe);
				}
				if (Input.GetKeyUp(KeyCode.LeftArrow))
				{
					left = false;
					button.Add ("LeftArrowUp");
					timepoint.Add (time_fe);
				}
				if (Input.GetKey(KeyCode.RightArrow) && attack_1 == false && attack_2 == false && attack_3 == false && right == false && hit == false)
				{
					right = true;
					button.Add ("RightArrow");
					timepoint.Add (time_fe);
				}
				if (Input.GetKeyUp(KeyCode.RightArrow))
				{
					right = false;
					button.Add ("RightArrowUp");
					timepoint.Add (time_fe);
				}
				if (Input.GetKeyDown(KeyCode.Space) && jump == false)
				{
					rig.AddRelativeForce (Vector2.up * 跳躍高度);
					button.Add ("jump");
					timepoint.Add (time_fe);
				}

				if (Input.GetKeyDown(KeyCode.Q) && attack_1 == false)
				{
					attack_1 = true;
					button.Add ("attack_1");
					timepoint.Add (time_fe);
				}
				if (Input.GetKeyDown(KeyCode.E) && attack_2 == false)
				{
					attack_2 = true;
					button.Add ("attack_2");
					timepoint.Add (time_fe);
				}
				if (Input.GetKeyDown(KeyCode.W) && attack_3 == false)
				{
					attack_3 = true;
					button.Add ("attack_3");
					timepoint.Add (time_fe);
				}
			}
		}

	}
	void ani_fn()
	{
		if (hit)
		{
			ani.Play ("hit");
		}
		if (attack_1 && attack_check == 0 || jump && attack_1) 
		{
			ani.Play ("attack_1");
		}
		if (attack_2 && attack_check == 0 && jump == false) 
		{
			ani.Play ("attack_2");
		}
		if (attack_3 && attack_check == 0 && jump == false) 
		{
			ani.Play ("attack_3");
		}
		if (jump && attack_1 == false)
		{
			ani.Play ("jump");
		}
		if (left && attack_1 == false && jump == false) 
		{
			ani.Play ("walk");
		}
		if (right && attack_1 == false && jump == false) 
		{
			ani.Play ("walk");
		}
		if (jump == false && left == false && right == false && attack_1 == false && attack_2 == false && attack_3 == false && hit == false)
		{
			ani.Play ("idle");
		}
	}
	void OnCollisionStay2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "ground" && jump && hit == false) 
		{
			jump = false;
			jump_par_check = 0;
		} 
	}
	void OnCollisionExit2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "ground" && jump == false && hit == false) 
		{
			jump = true;
		}
	}
	public void attparti_1(GameObject xx,float time)
	{
		GameObject att_par_1_cn = Instantiate (xx);
		att_par_1_cn.transform.SetParent (this.transform);
		att_par_1_cn.transform.localPosition = new Vector3 (0,0,0);
		att_par_1_cn.transform.localEulerAngles = new Vector3 (0,0,0);
		Destroy (att_par_1_cn,time);
	}
	private IEnumerator att1_wait_fin()
	{
		yield return new WaitForSeconds (key_Q);
		attack_1 = false;
		attack_check = 0;
	}
	private IEnumerator att2_wait_fin()
	{
		yield return new WaitForSeconds (key_E);
		attack_2 = false;
		attack_check = 0;
	}
	private IEnumerator att3_wait_fin()
	{
		yield return new WaitForSeconds (key_W);
		attack_3 = false;
		attack_check = 0;
	}
	private IEnumerator hit_cor()
	{
		yield return new WaitForSeconds (1f);
		hit = false;
		hit_check = 0;
	}
	private IEnumerator restart_wait()
	{
		yield return new WaitForSeconds (2f);
		SceneManager.LoadScene (0);
	}
}
