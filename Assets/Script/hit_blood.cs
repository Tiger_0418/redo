﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hit_blood : MonoBehaviour {

	public GameObject pler;
	GameObject mons_sc;

	// Use this for initialization
	void Start () 
	{
		pler = GameObject.FindGameObjectWithTag ("Player");
		mons_sc = GameObject.FindGameObjectWithTag ("mons");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "mons") 
		{
			if (this.tag == "forward_flash")
			{
				col.GetComponent<owl> ().血量 = col.GetComponent<owl> ().血量 - pler.GetComponent<player_main>().前斬傷害;
			}
			if (this.tag == "around_flash")
			{
				col.GetComponent<owl> ().血量 = col.GetComponent<owl> ().血量 - pler.GetComponent<player_main>().旋斬傷害;
			}
			if (this.tag == "up_flash")
			{
				col.GetComponent<owl> ().血量 = col.GetComponent<owl> ().血量 - pler.GetComponent<player_main>().上砍傷害;
			}
		}
		if (col.gameObject.tag == "Player") 
		{
			if (this.tag == "mons_skill")
			{
				pler.GetComponent<player_main> ().血量 = pler.GetComponent<player_main> ().血量 - mons_sc.GetComponent<owl> ().攻擊傷害;
				pler.GetComponent<player_main> ().hit = true;
			}
		}
	}
}
