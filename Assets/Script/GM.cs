﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour {

	public GameObject player_real;
	GameObject player_shadow;
	GameObject player_shadow_1;

//	public List<GameObject> shadow_real = new List<GameObject>();
	public GameObject[] shadow_go;

	public int die_check = 0;

	public Vector3 org_pos;
	public int inst_check = 0;

	public bool gmcheck;

	// Use this for initialization
	void Start () 
	{
		if (gmcheck == false)
		{
			DontDestroyOnLoad (this.gameObject);
			gmcheck = true;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		shadow_go = GameObject.FindGameObjectsWithTag ("head_Shadow");
		if (player_real == null)
		{
			player_real = GameObject.FindGameObjectWithTag ("head");
		}

		if (inst_check == 0)
		{
			for(int x=0;x<shadow_go.Length;x++)
			{
				GameObject shadow_go_cn = Instantiate (shadow_go[x]);
				shadow_go_cn.GetComponent<head>().player.SetActive(true);
			}
			inst_check = 1;
		}
			
		if (player_real.GetComponent<head>().player.GetComponent<player_main>().die && die_check == 0) 
		{
//			if (shadow_go.Length != 0)
//			{
//				for(int x=0;x<shadow_go.Length;x++)
//				{
//					DestroyImmediate (shadow_go[x]);
//				}
//			}

			//生成主體
			player_shadow = Instantiate (player_real);
			player_shadow.tag = "head_Shadow";
			player_shadow.GetComponent<head>().player.tag = "Shadow";
			player_shadow.GetComponent<head>().player.GetComponent<player_main>().time_f = 0;
			player_shadow.GetComponent<head>().player.GetComponent<player_main>().time_fe = 0;
			player_shadow.GetComponent<head> ().player.GetComponent<player_main> ().die = false;
			player_shadow.GetComponent<head> ().player.GetComponent<player_main> ().left = false;
			player_shadow.GetComponent<head> ().player.GetComponent<player_main> ().right = false;
			player_shadow.GetComponent<head> ().player.GetComponent<player_main> ().attack_1 = false;
			player_shadow.GetComponent<head> ().player.GetComponent<player_main> ().attack_2 = false;
			player_shadow.GetComponent<head> ().player.GetComponent<player_main> ().attack_3 = false;
			player_shadow.GetComponent<head> ().player.GetComponent<player_main> ().hit = false;
			player_shadow.GetComponent<head> ().player.GetComponent<player_main> ().血量 = 100;
			player_shadow.GetComponent<head> ().player.GetComponent<SpriteRenderer> ().color = new Color (255,0,0);

			player_shadow.transform.position = this.transform.position;
			player_shadow.GetComponent<head> ().transform.localPosition = org_pos;
			player_shadow.GetComponent<head> ().player.transform.localPosition = new Vector3(0,0,0);
			player_shadow.GetComponent<head>().player.layer = 8;
			player_shadow.GetComponent<head> ().player.SetActive (false);

			die_check = 1;
			DontDestroyOnLoad (player_shadow);
		}
//		if (Input.GetKeyDown(KeyCode.U))
//		{
//			if (shadow_go.Length != 0)
//			{
//				for(int x=0;x<shadow_go.Length;x++)
//				{
//					DestroyImmediate (shadow_go[x]);
//				}
//			}
//
//
//			//生成主體
//			player_real.transform.position = new Vector3 (-0.31f,0.928f,0);
//			player_real.GetComponent<player_main> ().time_f = 0;
//			player_shadow = Instantiate (player_real);
//			player_shadow.tag = "Shadow";
//			player_shadow.GetComponent<player_main> ().time_fe = 0;
//			player_shadow.layer = 8;
//			player_shadow.SetActive (false);
//			shadow_real.Add (player_shadow);
//			player_real.GetComponent<player_main> ().timepoint.Clear();
//
//			//生成殘影
//			for(int x=0;x<shadow_real.Count;x++)
//			{
//				player_shadow_1 = Instantiate (shadow_real[x]);
//				player_shadow_1.GetComponent<SpriteRenderer> ().material.color = new Color (255, 255, 255);
//				player_shadow_1.SetActive (true);
//			}
//		}
	}
}
