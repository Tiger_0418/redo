﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class owl : MonoBehaviour {

	public float 血量 = 100;
	public float 攻擊傷害 = 10f;

	public bool 待機;
	public bool 巡邏;
	public float 巡邏距離 = 5f;
	public float 巡邏速度 = 0.02f;
	public bool 攻擊;
	public float 攻擊速度 = 0.05f;
	public bool 追擊;
	public float 追擊高度 = 1f;
	public float 追擊速度 = 0.002f;
	public bool hit;
	public bool hit_back;
	Vector3 backpos;
	public bool die;



	Animator ani;
	public GameObject owl_ob;
	public GameObject owl_attparti;

	int check = 0;
	public GameObject target;
	Vector3 att_target_point;
	Vector3 hold_point;

	public float dis;
	float color_a = 1;

	Vector3 leftpoint;
	Vector3 rightpoint;
	bool trun;
	public bool fogjump;
	int fogjump_check = 0;


	// Use this for initialization
	void Start () 
	{
		ani = owl_ob.GetComponent<Animator> ();
		leftpoint = new Vector3 (transform.position.x - 巡邏距離, transform.position.y, transform.position.z);
		rightpoint = new Vector3 (transform.position.x + 巡邏距離, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (dis > 20)
		{
			巡邏 = true;
			追擊 = false;
			dis = 0;
			target = null;
		}

		if (hit)
		{
			ani.Play ("hit");
			owl_ob.GetComponent<SpriteRenderer> ().color = new Color(255,0,0);
			StartCoroutine (color_change());
			backpos = new Vector3 (transform.position.x+2f,transform.position.y,transform.position.z);
			hit = false;
			//hit_back = true;
		}
		if (hit_back) 
		{
			攻擊 = false;
			追擊 = false;
			this.transform.position = Vector3.Lerp (transform.position, backpos,0.1f);
		}
		if (血量 <= 0)
		{
			die = true;
		}
		if (die) 
		{
			color_a = color_a - 0.01f;
			owl_ob.GetComponent<SpriteRenderer> ().color = new Color(255,255,255,color_a);
			owl_ob.GetComponent<Animator> ().enabled = false;
			if (color_a <= 0)
			{
				Destroy (this.gameObject);
			}
		} 
		else 
		{
			if (待機)
			{
				ani.Play ("idle");
			}
			if (巡邏)
			{
				if (this.name == "owl" || this.name == "horse") 
				{
					ani.Play ("idle");
					if (this.transform.position.x > leftpoint.x && trun == false)
					{
						transform.Translate (Vector3.left*巡邏速度);
						this.transform.localEulerAngles = new Vector3 (0, 0, 0);
					}
					if (this.transform.position.x < leftpoint.x && trun == false)
					{
						trun = true;
					}
					if (this.transform.position.x < rightpoint.x && trun)
					{
						transform.Translate (Vector3.left*巡邏速度);
						this.transform.localEulerAngles = new Vector3 (0, -180, 0);
					}
					if (this.transform.position.x > rightpoint.x && trun)
					{
						trun = false;
					}
				}
				if (this.name == "fog" || this.name == "snail") 
				{
					if (fogjump == false && fogjump_check == 0) 
					{
						if (this.name == "fog")
						{
							ani.Play ("fog_jump");
						}
						if (this.name == "snail")
						{
							ani.Play ("snail_walk");
						}
						StartCoroutine (fogjump_hold(0.61f));
						fogjump_check = 1;
					}
					if (fogjump)
					{
						if (this.transform.position.x > leftpoint.x && trun == false)
						{
							transform.Translate (Vector3.left*巡邏速度);
							this.transform.localEulerAngles = new Vector3 (0, 0, 0);
						}
						if (this.transform.position.x < leftpoint.x && trun == false)
						{
							trun = true;
						}
						if (this.transform.position.x < rightpoint.x && trun)
						{
							transform.Translate (Vector3.left*巡邏速度);
							this.transform.localEulerAngles = new Vector3 (0, -180, 0);
						}
						if (this.transform.position.x > rightpoint.x && trun)
						{
							trun = false;
						}
					}
				}
			}
			if (攻擊) 
			{
				if (this.name == "fog" && fogjump == false) 
				{
					if (check == 0) 
					{
						ani.Play ("fog_blow");
						Destroy (this.gameObject,2f);
						check = 1;
					}
				}
				if (this.name == "owl")
				{
					if (check == 0) 
					{
						ani.Play ("attack");
						att_target_point = new Vector3 (target.transform.position.x, target.transform.position.y - 3, target.transform.position.z);
						GameObject owl_attparti_cn = Instantiate (owl_attparti);
						owl_attparti_cn.transform.SetParent (this.gameObject.transform);
						owl_attparti_cn.transform.localPosition = new Vector3 (0.1f, -0.75f, 0);
						check = 1;
						Destroy (owl_attparti_cn, 1f);
						StartCoroutine (att1_wait_fin (1.5f));
					}
					this.transform.position = Vector3.Lerp (transform.position, att_target_point, 攻擊速度);
				}
				if (this.name == "horse")
				{
					if (check == 0) 
					{
						ani.Play ("follow_2");
						GameObject owl_attparti_cn = Instantiate (owl_attparti);
						owl_attparti_cn.transform.SetParent (this.gameObject.transform);
						owl_attparti_cn.transform.localPosition = new Vector3 (0,-0.03f,0);
						owl_attparti_cn.transform.localEulerAngles = new Vector3 (0,-180,0);
						Destroy (owl_attparti_cn, 1.3f);
						StartCoroutine (att1_wait_fin (2f));
						check = 1;
					}
					this.transform.position = Vector3.Lerp (transform.position, att_target_point, 0.1f);
					//this.transform.position = new Vector3 (transform.position.x,1,transform.position.z);
				}
			}
			if (追擊) 
			{
				
				if (this.name == "fog") 
				{
					dis = Vector3.Distance(target.transform.position,transform.position);
					if (dis <= 追擊高度) 
					{
						攻擊 = true;
						追擊 = false;
					} 
					else 
					{ 
						if (fogjump == false && fogjump_check == 0) 
						{
							ani.Play ("fog_jump");
							StartCoroutine (fogjump_hold(0.3f));
							fogjump_check = 1;
						}
						if (fogjump)
						{
							if (this.transform.position.x > target.transform.position.x)
							{
								transform.Translate (Vector3.left*追擊速度);
								this.transform.localEulerAngles = new Vector3 (0, 0, 0);
							}
							if (this.transform.position.x < target.transform.position.x)
							{
								transform.Translate (Vector3.left*追擊速度);
								this.transform.localEulerAngles = new Vector3 (0, -180, 0);
							}
						}
					}
				}
				if (this.name == "owl") 
				{
					ani.Play ("idle");
					dis = Vector3.Distance(hold_point,transform.position);
					if (dis <= 追擊高度) 
					{
						攻擊 = true;
						追擊 = false;
					} 
					else 
					{
						if (target.transform.position.x < this.transform.position.x)
						{
							this.transform.localEulerAngles = new Vector3 (0, 0, 0);
						}
						if (target.transform.position.x >= this.transform.position.x)
						{
							this.transform.localEulerAngles = new Vector3 (0, -180, 0);
						}
						hold_point = new Vector3 (target.transform.position.x,target.transform.position.y+5,target.transform.position.z);
						this.transform.position = Vector3.Lerp (transform.position,hold_point,追擊速度);
					}
				}
				if (this.name == "horse") 
				{
					ani.Play ("follow");
					dis = Vector3.Distance(target.transform.position,transform.position);
					if (target.transform.position.x < this.transform.position.x)
					{
						this.transform.localEulerAngles = new Vector3 (0, 0, 0);
					}
					if (target.transform.position.x >= this.transform.position.x)
					{
						this.transform.localEulerAngles = new Vector3 (0, -180, 0);
					}
					if (dis <= 追擊高度) 
					{
						if (target.transform.position.x < this.transform.position.x)
						{
							att_target_point = new Vector3 (target.transform.position.x-10f,this.transform.position.y,0);
						}
						if (target.transform.position.x >= this.transform.position.x)
						{
							att_target_point = new Vector3 (target.transform.position.x+10f,this.transform.position.y,0);
						}
						攻擊 = true;
						追擊 = false;

					} 
					else 
					{ 
						
						transform.Translate(Vector2.left*追擊速度,Space.Self);
						追擊速度 += 0.001f;
					}
				}
			}
		}
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player" && 攻擊 == false) 
		{
			追擊 = true;
			巡邏 = false;
			target = col.gameObject;
		} 
	}

	private IEnumerator att1_wait_fin(float xx)
	{
		yield return new WaitForSeconds (xx);
		攻擊 = false;
		追擊 = true;
		check = 0;
		if (this.name == "horse")
		{
			追擊速度 = 巡邏速度;
		}
	}
	private IEnumerator color_change()
	{
		yield return new WaitForSeconds (0.03f);
		owl_ob.GetComponent<SpriteRenderer> ().color = new Color(255,255,255);
		yield return new WaitForSeconds (0.5f);
		hit_back = false;
		ani.Play ("idle");
	}
	private IEnumerator fogjump_hold(float xx)
	{
		yield return new WaitForSeconds (0.1f);
		fogjump = true;
		yield return new WaitForSeconds (xx);
		fogjump = false;
		ani.Play ("fog_ground");
		yield return new WaitForSeconds (1f);
		fogjump_check = 0;
	}
}
