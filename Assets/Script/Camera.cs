﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

	public GameObject actor;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		Vector3 cameraPos = Vector3.Lerp (transform.position, actor.transform.position, 2f*Time.deltaTime);
		transform.position = new Vector3 (cameraPos.x, cameraPos.y, -10);
	}
}
